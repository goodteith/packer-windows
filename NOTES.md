# Notes

The build process runs out of /tmp on my machines. Using a local directory for
the tmp files gets around this problem:

```bash
VAGRANT_CLOUD_TOKEN=$(pass apikeys/vagrant-cloud/packer-build | head -n1) \
  TMPDIR=~/.cache/packer packer build \
  -only=virtualbox-iso \
  -var iso_url=/home/stooj/downloads/images/en_windows_7_ultimate_with_sp1_x64_dvd_u_677332.iso \
  -var iso_checksum=c9f7ecb768acb82daacf5030e14b271e \
  -var version=0.0.2 \
  -var headless=false \
  windows_7.json

```
